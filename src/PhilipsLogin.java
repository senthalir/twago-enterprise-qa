import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import pages.MspDashboardPage;
import pages.ResetPasswordPage;
import pages.SignupPage;
import pages.DashboardPage;
import pages.ForgotPasswordPage;
import utilities.BrowserFactory;
import utilities.MailHelper;
import utilities.PropertyHelper;

public class PhilipsLogin {
	protected static WebDriver driver;

	protected static WebDriverWait wait;
	protected static LoginPage loginPage;
	protected static HomePage homePage;
	protected static ForgotPasswordPage forgotPasswordPage;
	protected static ResetPasswordPage resetPasswordPage;
	protected static DashboardPage dashBoardPage;
	protected static MspDashboardPage mspdashBoardPage;
	private static String currentUsername;
	private static String currentPassword;
	private static Boolean mspPage = false;
	protected static String mailId1;
	protected static String password1;
	protected static String baseMSPUrl;
	protected static String baseFreelancerUrl;
	protected static String wrongUserName;
	protected static String wrongPassword;
	protected static String correctPassword;
	protected static String browserDriverUrl;

	@BeforeClass
	@Parameters({ "browser", "isMSPPage" })
	public static void setup(@Optional("firefox") String browser, @Optional("false") String isMSPPage)
			throws Exception {
	
		mspPage = Boolean.parseBoolean(isMSPPage);
		if(!mspPage)
		{
		currentUsername = PropertyHelper.GetProperty("currentUserName");
		currentPassword = PropertyHelper.GetProperty("currentPassword");
		}
		else
		{
			currentUsername = PropertyHelper.GetProperty("currentMSPUserName");
			currentPassword = PropertyHelper.GetProperty("currentMSPPassword");
		}
		mailId1 = PropertyHelper.GetProperty("mailId");
		password1 = PropertyHelper.GetProperty("credentials");
		baseMSPUrl = PropertyHelper.GetProperty("baseMSPUrl");
		baseFreelancerUrl = PropertyHelper.GetProperty("baseFreelancerUrl");
		wrongUserName = PropertyHelper.GetProperty("wrongUserName");
		wrongPassword = PropertyHelper.GetProperty("wrongPassword");
		correctPassword = PropertyHelper.GetProperty("correctPassword");
		browserDriverUrl = PropertyHelper.GetProperty(browser + "DriverUrl");
		driver = BrowserFactory.getWebDriver(browser, browserDriverUrl, mspPage);
		wait = new WebDriverWait(driver, 60);
		BrowserFactory.LoadWebPage("/", true);
	}

	@Test(priority = 101)
	public void TC_Login_002() {
		if (!mspPage) {
			homePage = new HomePage(driver);
			homePage.clicklogIn();
		}
		loginPage = new LoginPage(driver);
		loginPage.setEmail(wrongUserName);
		loginPage.setPassword(wrongPassword);
		loginPage.clickSubmit(false);
		Assert.assertEquals(loginPage.getValidationError(), "Login or password not correct!");

	}

	@Test(priority = 102)
	public void TC_Login_003() {
		loginPage = new LoginPage(driver);
		loginPage.setEmail(currentUsername);
		loginPage.clearPassword();
		loginPage.clickSubmit(false);
		Assert.assertEquals(loginPage.getPasswordError(), "This field is required.");
	}

	@Test(priority = 103)
	public void TC_Login_004() {
		loginPage = new LoginPage(driver);
		loginPage.clearEmail();
		loginPage.setPassword(currentPassword);
		loginPage.clickSubmit(false);
		Assert.assertEquals(loginPage.getEmailError(), "This field is required.");
	}

	@SuppressWarnings("deprecation")
	@Test(priority = 106)
	public void TC_Login_005() {

		if (mspPage) {

			mspdashBoardPage = new MspDashboardPage(driver);
			mspdashBoardPage.clickLogout();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("form#loginForm")));
		} else {
			dashBoardPage = new DashboardPage(driver);
			dashBoardPage.clicklogout();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("figure.company-logo")));
			homePage = new HomePage(driver);
			homePage.clicklogIn();
		}

		loginPage = new LoginPage(driver);
		loginPage.clickForgotPassword();
		wait.until(ExpectedConditions.numberOfwindowsToBe(2));
		// get window handlers as list
		String oldTab = driver.getWindowHandle();
		List<String> browserTabs = new ArrayList<String>(driver.getWindowHandles());
		// switch to new tab
		int numberoftabs = browserTabs.size();
		System.out.println("number" + numberoftabs);
		// browserTabs.remove(oldTab);
		driver.switchTo().window(browserTabs.get(1));

		forgotPasswordPage = new ForgotPasswordPage(driver);
		forgotPasswordPage.setEmail(mailId1);
		forgotPasswordPage.clickRequestNewPassword();
		driver.switchTo().window(browserTabs.get(0));
		try {
			if (!mspPage) {
				String activationLink = MailHelper.GetUrl("forgot Password", baseFreelancerUrl, mailId1, password1);
				Assert.assertTrue(!activationLink.isEmpty());
				BrowserFactory.LoadWebPage(activationLink, false);
			} else {
				String activationLink = MailHelper.GetUrl("forgot Password", baseMSPUrl, mailId1, password1);
				Assert.assertTrue(!activationLink.isEmpty());
				BrowserFactory.LoadWebPage(activationLink, false);
			}

			resetPasswordPage = new ResetPasswordPage(driver);
			resetPasswordPage.setPassword(correctPassword);
			resetPasswordPage.setConfirmPassword(correctPassword);
			System.out.print("helo");
			resetPasswordPage.clickChangePassword();

		} catch (Exception E) {
			System.out.println(E);
		}

	}

	@Test(priority = 104)
	public void TC_Login_006() {
		BrowserFactory.LoadWebPage("/", true);
		if (!mspPage) {
			homePage = new HomePage(driver);
			homePage.clicklogIn();
		}
		loginPage = new LoginPage(driver);
		loginPage.setEmail(currentUsername);
		loginPage.setPassword(wrongPassword);
		loginPage.clickSubmit(false);
		Assert.assertEquals(loginPage.getValidationError(), "Login or password not correct!");
	}

	@Test(priority = 105)
	public void TC_Login_001() {

		loginPage = new LoginPage(driver);
		loginPage.setEmail(currentUsername);
		loginPage.setPassword(currentPassword);
		if (!mspPage) {
			loginPage.clickSubmit(true);
		} else {
			loginPage.clickMspSubmit();
		}
	}

	@Test(priority = 106)
	public void TC_Login_007() {

		if (!mspPage) {
			BrowserFactory.LoadWebPage("/", true);
			homePage = new HomePage(driver);
			homePage.clicklogIn();
			loginPage = new LoginPage(driver);
			loginPage.setEmail(SignupPage.emailString);
			loginPage.setPassword(currentPassword);
			loginPage.clickSubmit(true);
		}
	}

	@AfterClass
	public static void teardown() {
		driver.close();
		driver.quit();
	}

}
