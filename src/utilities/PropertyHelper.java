package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

public class PropertyHelper {
	

		private static Properties prop;

		public static String GetProperty(String propertyName)
		{
			if(prop == null) {
				File file = new File("/Users/kashme/work/Philips-Freelancing/input/credentials.properties");
				FileInputStream fileInput=null;
				try
				{
					fileInput = new FileInputStream(file);
				}catch(
						FileNotFoundException e)
				{
					e.printStackTrace();
				}
				prop = new Properties();
				try
				{
					prop.load(fileInput);
				}catch(
						IOException exception)
				{
					exception.printStackTrace();
				}
			}

			return prop.getProperty(propertyName);
		}
	}
