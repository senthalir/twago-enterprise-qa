package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ActivateFreelancerPage {
	protected static WebDriver driver;
	protected static WebDriverWait wait;

	public ActivateFreelancerPage(WebDriver wd) {
		driver = wd;
		PageFactory.initElements(wd, this);
		wait = new WebDriverWait(wd, 60);
	}
	@FindBy(how = How.CSS, using = ".btn-primary-big.modal-action-button")
	@CacheLookup
	WebElement modalButton;
	

	 public void clickModalOk()
	   {
		 modalButton.click();
		 wait.until(ExpectedConditions.urlContains("/dashboard"));
		
	   }
}
