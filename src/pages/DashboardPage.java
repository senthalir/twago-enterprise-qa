package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DashboardPage {
	protected static WebDriver driver;
	protected static WebDriverWait wait;

	public DashboardPage(WebDriver wd) {
		driver = wd;
		PageFactory.initElements(wd, this);
		wait = new WebDriverWait(wd, 60);
	}


	
	@FindBy(how = How.CSS, using = "a[href='/logout']")
	@CacheLookup
	WebElement logout;
	
	@FindBy(how = How.CSS, using = "a[href='/job']")
	@CacheLookup
	WebElement work;
	
	@FindBy(how = How.CSS, using = "a[href='/profile']")
	@CacheLookup
	WebElement myProfile;
	
	@FindBy(how = How.CSS, using = "a[href='/profile/edit']")
	@CacheLookup
	WebElement editProfile;
	
	@FindBy(how = How.CLASS_NAME, using = "js-profile-trigger active")
	@CacheLookup
	WebElement publicProfile;
	
	@FindBy(how = How.CLASS_NAME, using = "js-resume-trigger")
	@CacheLookup
	WebElement editCV;
	
	@FindBy(how = How.CSS, using = "a[href='/profile#resumePage']")
	@CacheLookup
	WebElement updateCV;
	
	@FindBy(how = How.CSS, using = "a[href='/pool']")
	@CacheLookup
	WebElement pool;
	
	
	 public void clicklogout()
	   {
		
		logout.sendKeys(Keys.ENTER);
		
		//logout.click() ;
	   }
	 public void clickWork()
	   {
		
		work.sendKeys(Keys.ENTER);
		 wait.until(ExpectedConditions.titleContains("job"));
	   }
	 public void clickProfile()
	   {
		
		myProfile.sendKeys(Keys.ENTER);
		 wait.until(ExpectedConditions.titleContains("Public profile"));
		
	   }
	 public void clickEditProfile()
	   {
		
		 editProfile.sendKeys(Keys.ENTER);
		 wait.until(ExpectedConditions.titleContains("Edit profile"));
		
	   }
	 public void clickPublicProfile()
	   {
		
		 publicProfile.sendKeys(Keys.ENTER);
		 wait.until(ExpectedConditions.titleContains("Public profile"));
		
	   }
	 public void clickEditCV()
	   {
		
		 editCV.sendKeys(Keys.ENTER);
		 wait.until(ExpectedConditions.titleContains("profile#resumePage"));
	   }
	 
}
