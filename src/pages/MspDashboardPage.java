package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MspDashboardPage {
	protected static WebDriver driver;
	protected static WebDriverWait wait;

	public MspDashboardPage(WebDriver wd) {
		driver = wd;
		PageFactory.initElements(wd, this);
		wait = new WebDriverWait(wd, 60);
	}


	@FindBy(how = How.CSS, using = "a[href='/logout']")
	@CacheLookup
	WebElement logout;
	
	@FindBy(how = How.CSS, using = "a[href='/freelancer']")
	@CacheLookup
	WebElement freelancer;
	@FindBy(how = How.CSS, using = "a[href='/pool']")
	@CacheLookup
	WebElement pool;
	@FindBy(how = How.CSS, using = "a[href='/job']")
	@CacheLookup
	WebElement job;
	
	 public void clickLogout()
	   {
		// logout.click();
		  logout.sendKeys(Keys.ENTER);
		 // wait.until(ExpectedConditions.urlContains("login"));
		
	   }
	 public void clickFreelancer()
	   {
		//freelancer.click();
		freelancer.sendKeys(Keys.ENTER);
		 wait.until(ExpectedConditions.titleContains("Freelancer list"));
	   }
	 public void clickPool()
	   {
		pool.click();
		
	   }
	 public void clickJob()
	 {
		 job.click();
	 }
}
