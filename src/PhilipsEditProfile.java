import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import pages.MspDashboardPage;
import pages.SignupPage;
import pages.TermsPage;
import pages.ActivateFreelancerPage;
import pages.DashboardPage;
import pages.EditProfilePage;
import pages.FreelancerListPage;
import utilities.BrowserFactory;
import utilities.MailHelper;
import utilities.PropertyHelper;

public class PhilipsEditProfile {
	protected static WebDriver driver;
	protected static WebDriverWait wait;
	protected static LoginPage loginPage;
	protected static HomePage homePage;
	protected static SignupPage signupPage;
	protected static TermsPage termsPage;
	protected static FreelancerListPage freelancerListPage;
	protected static MspDashboardPage mspdashboardPage;
	protected static ActivateFreelancerPage activateFreelancerPage;
	protected static EditProfilePage editProfilePage;
	protected static DashboardPage dashboardPage;

	private static Boolean mspPage = false;
	protected static String registerUserName;
	protected static String registerPassWord;
	protected static String correctPassword;
	protected static String baseFreelancerUrl;
	protected static String registerwrongMailid1;
	protected static String registerwrongMailid2;
	protected static String wrongPhoneNo;
	protected static String wrongPassword;
	private static String currentUsername;
	private static String currentPassword;
	protected static String incorrectRate;
	protected static String wrongvatid1;
	protected static String wrongvatid2;
	protected static String vatId;
	protected static String imgSource;
	protected static String fileSource;
	protected static String EditBirthday;
	protected static String EditBirthmonth;
	protected static String EditBirthyear;
	protected static String EditNationality;
	protected static String browserDriverUrl;

	@BeforeClass
	@Parameters({ "browser", "isMSPPage" })
	public static void setup(@Optional("firefox") String browser, @Optional("false") String isMSPPage)
			throws Exception {
		mspPage = Boolean.parseBoolean(isMSPPage);
		correctPassword = PropertyHelper.GetProperty("correctPassword");
		registerUserName = PropertyHelper.GetProperty("registerMailId");
		registerPassWord = PropertyHelper.GetProperty("registerPassword");
		baseFreelancerUrl = PropertyHelper.GetProperty("baseFreelancerUrl");
		registerwrongMailid1 = PropertyHelper.GetProperty("registerwrongMailid1");
		registerwrongMailid2 = PropertyHelper.GetProperty("registerwrongMailid2");
		wrongPhoneNo = PropertyHelper.GetProperty("wrongPhoneNo");
		wrongPassword = PropertyHelper.GetProperty("wrongPassword");
		currentUsername = PropertyHelper.GetProperty("editProfileMailId");
		currentPassword = PropertyHelper.GetProperty("editProfilePassword");
		incorrectRate = PropertyHelper.GetProperty("incorrectRate");
		wrongvatid1 = PropertyHelper.GetProperty("wrongvatid1");
		wrongvatid2 = PropertyHelper.GetProperty("wrongvatid2");
		vatId = PropertyHelper.GetProperty("vatid");
		imgSource = PropertyHelper.GetProperty("imgSource");
		fileSource = PropertyHelper.GetProperty("fileSource");
		EditBirthday = PropertyHelper.GetProperty("EditbirthDay");
		EditBirthmonth = PropertyHelper.GetProperty("EditbirthMonth");
		EditBirthyear = PropertyHelper.GetProperty("EditbirthYear");
		browserDriverUrl = PropertyHelper.GetProperty(browser + "DriverUrl");
		driver = BrowserFactory.getWebDriver(browser, browserDriverUrl, mspPage);
		BrowserFactory.LoadWebPage("/", true);
	}

	@Test(priority = 201)
	public void TC_EditProfile_002() {
		homePage = new HomePage(driver);
		homePage.clicklogIn();
		loginPage = new LoginPage(driver);
		loginPage.setEmail(SignupPage.emailString);
		loginPage.setPassword(currentPassword);
		loginPage.clickSubmit(true);
		dashboardPage = new DashboardPage(driver);
		dashboardPage.clickProfile();
		dashboardPage.clickEditProfile();

		editProfilePage = new EditProfilePage(driver);
		editProfilePage.setFirstName("kash1223");
		Assert.assertEquals(editProfilePage.getfirstNameError(), "Letters only");
	}

	@Test(priority = 202)
	public void TC_EditProfile_003() {

		editProfilePage = new EditProfilePage(driver);
		editProfilePage.setLastName("kashme1233");
		Assert.assertEquals(editProfilePage.getlastNameError(), "Letters only");

	}

	@Test(priority = 203)
	public void TC_EditProfile_004() {

		editProfilePage = new EditProfilePage(driver);
		editProfilePage.setEmail(registerwrongMailid1);
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.getemailError(), "Please enter a valid email address.");
	}

	@Test(priority = 204)
	public void TC_EditProfile_005() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.setEmail(registerwrongMailid2);
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.getemailError(), "Please enter a valid email address.");
	}

	@Test(priority = 206)
	public void TC_EditProfile_007() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.setMobileNumber(wrongPhoneNo);
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.getmobileError(), "Please enter only digits.");

	}

	@Test(priority = 207)
	public void TC_EditProfile_008() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.setPhoneNumber(wrongPhoneNo);
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.getphoneError(), "Please enter only digits.");
	}

	@Test(priority = 208)
	public void TC_EditProfile_009() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.getnationalityError(), "This field is required.");
	}

	@Test(priority = 209)
	public void TC_EditProfile_010() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.getcountryError(), "This field is required.");
	}

	@Test(priority = 210)
	public void TC_EditProfile_011() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.gethomeAddressLine1Error(), "This field is required.");
	}

	@Test(priority = 211)
	public void TC_EditProfile_012() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.getStreetError(), "This field is required.");
	}

	@Test(priority = 212)
	public void TC_EditProfile_013() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.getCityError(), "This field is required.");
	}

	@Test(priority = 213)
	public void TC_EditProfile_014() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.getZipError(), "This field is required.");
	}

	@Test(priority = 214)
	public void TC_EditProfile_015() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.setNonEuCitizen();
		Assert.assertEquals(editProfilePage.verifyNonEUElements(), true);

	}

	@Test(priority = 215)
	public void TC_EditProfile_016() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.sethourRateValue(incorrectRate);
		Assert.assertEquals(editProfilePage.gethourRateError(), "Please enter only digits.");

	}

	@Test(priority = 216)
	public void TC_EditProfile_017() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.choosediffbillingAddressOption();
		Assert.assertEquals(editProfilePage.verifydiffbillingAddressElements(), true);
	}

	@Test(priority = 217)
	public void TC_EditProfile_018() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.choosediffbillingAddressOption();
		editProfilePage.clickSubmit();
		// Assert.assertEquals(EditProfilePage.getBillingCountryError(), "This
		// field is required.");
	}

	@Test(priority = 218)
	public void TC_EditProfile_019() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.choosediffbillingAddressOption();
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.getBillingAddressLine1Error(), "This field is required.");
	}

	@Test(priority = 219)
	public void TC_EditProfile_020() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.choosediffbillingAddressOption();
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.getBillingStreetError(), "This field is required.");
	}

	@Test(priority = 220)
	public void TC_EditProfile_021() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.choosediffbillingAddressOption();
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.getBillingCityError(), "This field is required.");
	}

	@Test(priority = 221)
	public void TC_EditProfile_022() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.choosediffbillingAddressOption();
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.getBillingZipError(), "This field is required.");
	}

	@Test(priority = 222)
	public void TC_EditProfile_023() {
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.inputValidData();
		editProfilePage.setvatId(wrongvatid1);
		editProfilePage.clickSubmit();
		Assert.assertEquals(editProfilePage.getVatIDError(), "VAT is invalid");
	}

	// @Test(priority = 223)
	// public void TC_EditProfile_024() {
	// EditProfilePage =new EditProfilePage(driver);
	// // EditProfilePage.fileUpload(fileSource);
	// Assert.assertEquals(EditProfilePage.getUploadFilesInfo(), "Attached files
	// will not be send to the client.");
	//
	// }
	// @Test(priority = 224)
	// public void TC_EditProfile_025() {
	// EditProfilePage =new EditProfilePage(driver);
	// //EditProfilePage.imageUpload(imgSource);
	// //Assert.assertEquals(EditProfilePage.getUploadFilesInfo(), "Attached
	// files will not be send to the client.");
	//
	// }
	
	@Test(priority = 225)
	public void TC_EditProfile_006() {

		dashboardPage = new DashboardPage(driver);
		dashboardPage.clickProfile();
		dashboardPage.clickEditProfile();
		EditProfilePage editPage = new EditProfilePage(driver);
		editPage.inputValidData();
		editPage.setBirthDay(EditBirthday);
		editPage.setBirthMonth(EditBirthmonth);
		editPage.setBirthYear(EditBirthyear);
		editPage.clickSubmit();
		//Assert.assertEquals(editPage.getSuccessMessage(), "Freelancer has been updated");
		dashboardPage = new DashboardPage(driver);
		dashboardPage.clickProfile();
		dashboardPage.clickEditProfile();
		editPage = new EditProfilePage(driver);
		Assert.assertEquals(editPage.getbirthDate(), EditBirthday);
		Assert.assertEquals(editPage.getbirthMonth(), EditBirthmonth);
		Assert.assertEquals(editPage.getbirthYear(), EditBirthyear);
	}
	
	@Test(priority = 226)
	public void TC_EditProfile_026() {

		dashboardPage = new DashboardPage(driver);
		dashboardPage.clickProfile();
		dashboardPage.clickEditProfile();
		
		editProfilePage = new EditProfilePage(driver);
		editProfilePage.inputValidData();
		editProfilePage.setvatId(vatId);
		// EditProfilePage.imageUpload(imgSource);
		editProfilePage.clickSubmit();

	}

	@AfterClass
	public static void teardown() {
		driver.close();
		driver.quit();
	}

}
