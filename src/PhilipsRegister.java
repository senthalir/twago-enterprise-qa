import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import pages.SignupPage;
import pages.TermsPage;
import pages.ActivateFreelancerPage;
import utilities.BrowserFactory;
import utilities.MailHelper;
import utilities.PropertyHelper;

public class PhilipsRegister {
	protected static WebDriver driver;

	protected static WebDriverWait wait;
	protected static LoginPage loginPage;
	protected static HomePage homePage;
	protected static SignupPage signupPage;
	protected static TermsPage termsPage;
	protected static ActivateFreelancerPage activateFreelancerPage;
	private static Boolean mspPage = false;
	protected static String registerUserName;
	protected static String registerPassWord;
	protected static String correctPassword;
	protected static String baseFreelancerUrl;
	protected static String registerwrongMailid1;
	protected static String registerwrongMailid2;
	protected static String wrongPhoneNo;
	protected static String wrongPassword;
	protected static String browserDriverUrl;

	@BeforeClass
	@Parameters({ "browser", "isMSPPage" })
	public static void setup(@Optional("firefox") String browser, @Optional("false") String isMSPPage)
			throws Exception {
		mspPage = Boolean.parseBoolean(isMSPPage);
		correctPassword = PropertyHelper.GetProperty("correctPassword");
		registerUserName = PropertyHelper.GetProperty("registerMailId");
		registerPassWord = PropertyHelper.GetProperty("registerPassword");
		baseFreelancerUrl = PropertyHelper.GetProperty("baseFreelancerUrl");
		registerwrongMailid1 = PropertyHelper.GetProperty("registerwrongMailid1");
		registerwrongMailid2 = PropertyHelper.GetProperty("registerwrongMailid2");
		wrongPhoneNo = PropertyHelper.GetProperty("wrongPhoneNo");
		wrongPassword = PropertyHelper.GetProperty("wrongPassword");
		browserDriverUrl = PropertyHelper.GetProperty(browser + "DriverUrl");
		driver = BrowserFactory.getWebDriver(browser, browserDriverUrl, mspPage);
		BrowserFactory.LoadWebPage("/", true);
	}

	@Test(priority = 1)
	public void TC_Registration_002() {

		homePage = new HomePage(driver);
		homePage.clickSignUp();
		signupPage = new SignupPage(driver);
		signupPage.setFirstName("kash1223");
		Assert.assertEquals(signupPage.getfirstNameError(), "Letters only");

	}

	@Test(priority = 2)
	public void TC_Registration_003() {

		signupPage = new SignupPage(driver);
		signupPage.setLastName("kashme1233");
		Assert.assertEquals(signupPage.getlastNameError(), "Letters only");

	}

	@Test(priority = 3)
	public void TC_Registration_004() {
		// homePage = new HomePage(driver);
		// homePage.clickSignUp();
		signupPage = new SignupPage(driver);
		signupPage.setEmail(registerwrongMailid1);
		signupPage.clickSignup();
		Assert.assertEquals(signupPage.getemailError(), "Please enter a valid email address.");
	}

	@Test(priority = 4)
	public void TC_Registration_005() {
		signupPage = new SignupPage(driver);
		signupPage.setEmail(registerwrongMailid2);
		signupPage.clickSignup();
		Assert.assertEquals(signupPage.getemailError(), "Please enter a valid email address.");
	}

	@Test(priority = 5)
	public void TC_Registration_006() {
		signupPage = new SignupPage(driver);
		signupPage.clickSignup();
		Assert.assertEquals(signupPage.getbirthDaySingleError(), "This field is required.");
		Assert.assertEquals(signupPage.getbirthMonthError(), "This field is required.");
		Assert.assertEquals(signupPage.getbirthYearError(), "This field is required.");
	}

	@Test(priority = 6)
	public void TC_Registration_007() {
		signupPage = new SignupPage(driver);
		signupPage.setMobileNumber(wrongPhoneNo);
		signupPage.clickSignup();
		Assert.assertEquals(signupPage.getmobileError(), "Please enter only digits.");
	}

	@Test(priority = 7)
	public void TC_Registration_008() {
		signupPage = new SignupPage(driver);
		signupPage.setPhoneNumber(wrongPhoneNo);
		signupPage.clickSignup();
		Assert.assertEquals(signupPage.getphoneError(), "Please enter only digits.");
	}

	@Test(priority = 8)
	public void TC_Registration_009() {
		signupPage = new SignupPage(driver);
		signupPage.setPassword(wrongPassword);
		Assert.assertEquals(signupPage.getpasswordError(), "Must be at least 10 characters");
	}

	@Test(priority = 9)
	public void TC_Registration_010() {
		signupPage = new SignupPage(driver);
		signupPage.setPassword(correctPassword);
		signupPage.setConfirmPassword(correctPassword);
		signupPage.clickSignup();
		Assert.assertEquals(signupPage.getpassword(), signupPage.getpasswordConfirm());
	}

	@Test(priority = 10)
	public void TC_Registration_011() {
		signupPage = new SignupPage(driver);
		signupPage.setPassword(correctPassword);
		signupPage.setConfirmPassword(wrongPassword);
		Assert.assertEquals(signupPage.getpasswordConfirmError(), "Passwords not match.");
	}

	@Test(priority = 11)
	public void TC_Registration_012() {
		signupPage = new SignupPage(driver);
		signupPage.clickSignin();
	}

	@Test(priority = 12)
	public void TC_Registration_013() {
		loginPage = new LoginPage(driver);
		loginPage.clicksignUp();
		signupPage = new SignupPage(driver);
		signupPage.clickTerms();
		termsPage = new TermsPage(driver);
		termsPage.clickSignUp();
	}

	@Test(priority = 13)
	public void TC_Registration_001() {
		signupPage.inputValidData();
		signupPage.clickSignup();
	}

	@Test(priority = 14)
	public void TC_Registration_014() throws Exception {
		String activeBaseUrl = "http://elink.foobar.freelance-management.com";
		signupPage.verifySignup();
		try {
			String activationLink = MailHelper.GetUrl("Welcome to the Philips Freelance Platform", activeBaseUrl,
					registerUserName, registerPassWord);
			System.out.println(activationLink);
			Assert.assertTrue(!activationLink.isEmpty());
			BrowserFactory.LoadWebPage(activeBaseUrl + activationLink, false, true);
			activateFreelancerPage = new ActivateFreelancerPage(driver);
			// Assert.assertEquals(driver.getTitle(), "Account Activated");
			activateFreelancerPage.clickModalOk();
			;

		} catch (Exception E) {
			throw E;
		}
	}
	@Test(priority = 15)
	public void TC_Registration_015() throws Exception {
	Assert.assertEquals(MailHelper.attachment,"freelancer-tos-v1.pdf");
	}
	@AfterClass
	public static void teardown() {
		driver.close();
		driver.quit();
	}

}
