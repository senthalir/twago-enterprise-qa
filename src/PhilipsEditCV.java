import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import pages.MspDashboardPage;
import pages.SignupPage;
import pages.TermsPage;
import pages.ActivateFreelancerPage;
import pages.DashboardPage;
import pages.EditCVPage;
import pages.EditProfilePage;
import pages.FreelancerListPage;
import utilities.BrowserFactory;
import utilities.MailHelper;
import utilities.PropertyHelper;

public class PhilipsEditCV {
	protected static WebDriver driver;
	protected static WebDriverWait wait;
	protected static LoginPage loginPage;
	protected static HomePage homePage;
	protected static SignupPage signupPage;
	protected static TermsPage termsPage;
	protected static FreelancerListPage freelancerListPage;
	protected static MspDashboardPage mspdashboardPage;
	protected static ActivateFreelancerPage activateFreelancerPage;
	protected static EditCVPage editcvPage;
	protected static DashboardPage dashboardPage;

	private static Boolean mspPage = false;
	protected static String registerUserName;
	protected static String registerPassWord;
	protected static String correctPassword;
	protected static String baseFreelancerUrl;
	protected static String registerwrongMailid1;
	protected static String registerwrongMailid2;
	protected static String wrongPhoneNo;
	protected static String wrongPassword;
	private static String currentUsername;
	private static String currentPassword;
	protected static String resumeCity;
	protected static String resumeCountry;
	protected static String resumeNationality;
	protected static String resumeAvailability;
	protected static String resumePhone;
	protected static String resumeFirstName;
	protected static String resumeLastName;
	protected static String resumeSkill;
	protected static String resumeStatus;
	protected static String incorrectRate;
	protected static String wrongvatid1;
	protected static String wrongvatid2;
	protected static String vatId;
	protected static String imgSource;
	protected static String fileSource;
	protected static String EditBirthday;
	protected static String EditBirthmonth;
	protected static String EditBirthyear;
	protected static String EditNationality;
	protected static String browserDriverUrl;

	@BeforeClass
	@Parameters({ "browser", "isMSPPage" })
	public static void setup(@Optional("firefox") String browser, @Optional("false") String isMSPPage)
			throws Exception {
		mspPage = Boolean.parseBoolean(isMSPPage);
		correctPassword = PropertyHelper.GetProperty("correctPassword");
		registerUserName = PropertyHelper.GetProperty("registerMailId");
		registerPassWord = PropertyHelper.GetProperty("registerPassword");
		baseFreelancerUrl = PropertyHelper.GetProperty("baseFreelancerUrl");
		registerwrongMailid1 = PropertyHelper.GetProperty("registerwrongMailid1");
		registerwrongMailid2 = PropertyHelper.GetProperty("registerwrongMailid2");
		wrongPhoneNo = PropertyHelper.GetProperty("wrongPhoneNo");
		wrongPassword = PropertyHelper.GetProperty("wrongPassword");
		currentUsername = PropertyHelper.GetProperty("editProfileMailId");
		currentPassword = PropertyHelper.GetProperty("editProfilePassword");
		incorrectRate = PropertyHelper.GetProperty("incorrectRate");
		wrongvatid1 = PropertyHelper.GetProperty("wrongvatid1");
		wrongvatid2 = PropertyHelper.GetProperty("wrongvatid2");
		vatId = PropertyHelper.GetProperty("vatid");
		imgSource = PropertyHelper.GetProperty("imgSource");
		fileSource = PropertyHelper.GetProperty("fileSource");
		EditBirthday = PropertyHelper.GetProperty("EditbirthDay");
		EditBirthmonth = PropertyHelper.GetProperty("EditbirthMonth");
		EditBirthyear = PropertyHelper.GetProperty("EditbirthYear");
		resumeCity = PropertyHelper.GetProperty("city");
		resumeCountry = PropertyHelper.GetProperty("country");
		resumeNationality=PropertyHelper.GetProperty("nationality");
		resumeAvailability=PropertyHelper.GetProperty("availability");
	    resumePhone=PropertyHelper.GetProperty("correctPhoneNo");
	    resumeFirstName=PropertyHelper.GetProperty("firstName");
	    resumeLastName=PropertyHelper.GetProperty("lastName");
	    resumeStatus=PropertyHelper.GetProperty("status");
	    resumeSkill=PropertyHelper.GetProperty("skill");
		browserDriverUrl = PropertyHelper.GetProperty(browser + "DriverUrl");
		driver = BrowserFactory.getWebDriver(browser, browserDriverUrl, mspPage);
		BrowserFactory.LoadWebPage("/", true);
	}

	@Test(priority = 301)
	public void TC_EditCV_001() {
		homePage = new HomePage(driver);
		homePage.clicklogIn();
		loginPage = new LoginPage(driver);
		loginPage.setEmail(SignupPage.emailString);
		loginPage.setPassword(currentPassword);
		loginPage.clickSubmit(true);
		dashboardPage = new DashboardPage(driver);
		dashboardPage.clickProfile();
		dashboardPage.clickEditCV();
         String resumeLocation = resumeCity + "," +resumeCountry ; 
		editcvPage = new EditCVPage(driver);
		Assert.assertEquals(editcvPage.getResumeLocation(), resumeLocation);
	}

	@Test(priority = 302)
	public void TC_EditCV_002() {
		editcvPage = new EditCVPage(driver);
		Assert.assertEquals(editcvPage.getResumeNationality(), resumeNationality);

	}

	@Test(priority = 303)
	public void TC_EditCV_003() {

		editcvPage = new EditCVPage(driver);
		Assert.assertEquals(editcvPage.getResumeAvailability(), resumeAvailability);
	}
	@Test(priority = 304)
	public void TC_EditCV_004() {
		editcvPage = new EditCVPage(driver);
		Assert.assertEquals(editcvPage.getResumeMailId(), SignupPage.emailString);
	}


	@Test(priority = 305)
	public void TC_EditCV_005() {
		editcvPage = new EditCVPage(driver);
		Assert.assertEquals(editcvPage.getResumePhoneNo(), resumePhone);
	}

	@Test(priority = 306)
	public void TC_EditCV_006() {
		editcvPage = new EditCVPage(driver);
		String resumefullName = resumeFirstName + resumeLastName ;
		Assert.assertEquals(editcvPage.getResumeName(), resumefullName);	

	}

	@Test(priority = 307)
	public void TC_EditCV_007() {
		editcvPage = new EditCVPage(driver);
		Assert.assertEquals(editcvPage.getResumeAvailable(),resumeStatus);
	}

	@Test(priority = 308)
	public void TC_EditCV_008() {
		editcvPage = new EditCVPage(driver);
		editcvPage.addSkill();
		editcvPage.setSkill(resumeSkill);
		editcvPage.addSkillbutton();
		editcvPage.saveSkill();
	}

	@Test(priority = 309)
	public void TC_EditCV_009() {
		editcvPage = new EditCVPage(driver);
		
	}

	@Test(priority = 310)
	public void TC_EditCV_010() {
		editcvPage = new EditCVPage(driver);
	}

	@Test(priority = 311)
	public void TC_EditCV_011() {
		editcvPage = new EditCVPage(driver);
		editcvPage.addJob();
		//editcvPage.setprojectTitle(title);
	}


	@AfterClass
	public static void teardown() {
		driver.close();
		driver.quit();
	}

}
